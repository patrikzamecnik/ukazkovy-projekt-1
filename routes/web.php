<?php

Route::middleware('auth')->group(function () {
    Route::prefix('kiosk')->name('kiosk.')->group(function () {
        Route::get('/show', 'KioskController@getAll')
            ->name('getAll')
            ->middleware('can:viewAny,App\Models\Kiosk');

        Route::get('/create', 'KioskController@create')
            ->name('create')
            ->middleware('can:create,App\Models\Kiosk');

        Route::get('/{kiosk}/edit', 'KioskController@edit')
            ->name('edit')
            ->middleware('can:edit,App\Models\Kiosk,kiosk');

        Route::get('{id}/delete', 'KioskController@destroy')
            ->name('delete')
            ->middleware('can:delete,App\Models\Kiosk');

        Route::post('/store', 'KioskController@store')
            ->name('store')
            ->middleware('can:create,App\Models\Kiosk');

        Route::post('/update', 'KioskController@update')
            ->name('update')
            ->middleware('can:update,App\Models\Kiosk');
    });

    Route::prefix('transaction')->name('transaction.')->group(function () {
        Route::get('/{kiosk_id}/show', 'TransactionController@getAllByKioskId')
            ->name('getAllByKioskId')
            ->middleware('can:view,App\Models\Transaction,kiosk_id');

        Route::get('/show', 'TransactionController@getAll')
            ->name('getAll')
            ->middleware('can:viewAny,App\Models\Transaction');

        Route::post('/showFiltered', 'TransactionController@getAllFiltered')
            ->name('getAllFiltered')
            ->middleware('can:viewAny,App\Models\Transaction');
    });

    Route::prefix('user')->name('user.')->group(function () {
        Route::get('/show', 'UserController@getAll')
            ->name('getAll')
            ->middleware('can:viewAny,App\Models\User');

        Route::get('/create', 'UserController@create')
            ->name('create')
            ->middleware('can:create,App\Models\User');

        Route::get('/{id}/edit', 'UserController@edit')
            ->name('edit')
            ->middleware('can:update,App\Models\User');

        Route::get('{id}/delete', 'UserController@destroy')
            ->name('delete')
            ->middleware('can:delete,App\Models\User');

        Route::post('/store', 'UserController@store')
            ->name('store')
            ->middleware('can:create,App\Models\User');

        Route::patch('/update/{user}', 'UserController@update')
            ->name('update')
            ->middleware('can:update,App\Models\User');
    });
});

Auth::routes();

Route::auth(['register' => false]);
Route::get('/home', 'HomeController@index')
    ->name('home');

Route::get('/', function () {
    return redirect('home');
})->name('/');
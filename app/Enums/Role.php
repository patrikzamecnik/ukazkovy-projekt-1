<?php

namespace App\Enums;

class Role
{
    /*
     * Enumerator for roles
     */
    const ADMIN = 'admin';
    const CUSTOMER = 'customer';
}

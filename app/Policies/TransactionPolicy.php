<?php

namespace App\Policies;

use App\Enums\Role;
use App\Models\Kiosk;
use App\Models\User;
use App\Models\Transaction;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any transactions.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return in_array($user->role, [
            Role::CUSTOMER,
            Role::ADMIN,
        ]);
    }

    /**
     * Determine whether the user can view the transaction.
     *
     * @param User $user
     * @param int $kiosk_id
     * @return mixed
     */
    public function view(User $user, $kiosk_id)
    {
        $kiosk = new Kiosk();
        $kiosks = $kiosk->getAll();
        $correct_id = false;
        foreach ($kiosks as $row)
            if (strval($row->id) === $kiosk_id)
                $correct_id = true;


        $correct_role = in_array($user->role, [
            Role::CUSTOMER,
            Role::ADMIN,
        ]);

        if (($correct_role && $correct_id) || $user->role === Role::ADMIN)
            return true;
        return false;
    }

    /**
     * Determine whether the user can create transactions.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->role, [
            Role::ADMIN,
        ]);
    }

    /**
     * Determine whether the user can update the transaction.
     *
     * @param User $user
     * @param Transaction $transaction
     * @return mixed
     */
    public function update(User $user, Transaction $transaction)
    {
        return in_array($user->role, [
            Role::ADMIN,
        ]);
    }

    /**
     * Determine whether the user can delete the transaction.
     *
     * @param User $user
     * @param Transaction $transaction
     * @return mixed
     */
    public function delete(User $user, Transaction $transaction)
    {
        return in_array($user->role, [
            Role::ADMIN,
        ]);
    }

    /**
     * Determine whether the user can restore the transaction.
     *
     * @param User $user
     * @param Transaction $transaction
     * @return mixed
     */
    public function restore(User $user, Transaction $transaction)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the transaction.
     *
     * @param User $user
     * @param Transaction $transaction
     * @return mixed
     */
    public function forceDelete(User $user, Transaction $transaction)
    {
        return in_array($user->role, [
            Role::ADMIN,
        ]);
    }
}

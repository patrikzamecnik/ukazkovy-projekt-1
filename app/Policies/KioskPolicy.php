<?php

namespace App\Policies;

use App\Enums\Role;
use App\Models\User;
use App\Models\Kiosk;
use App\Models\UsersKiosk;
use Illuminate\Auth\Access\HandlesAuthorization;

class KioskPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any kiosks.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return in_array($user->role, [
            Role::CUSTOMER,
            Role::ADMIN,
        ]);
    }

    /**
     * Determine whether the user can view the kiosk.
     *
     * @param User $user
     * @return mixed
     */
    public function view(User $user)
    {
        return in_array($user->role, [
            Role::CUSTOMER,
            Role::ADMIN,
        ]);
    }

    /**
     * Determine whether the user can create kiosks.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->role, [
            Role::ADMIN,
        ]);
    }

    /**
     * @param User $user
     * @param int $id
     * @return bool
     */
    public function edit(User $user, Kiosk $kiosek)
    {
        $userKiosk = new UsersKiosk();
        $kiosk = new Kiosk();
        $kiosks = $kiosk->getAll();
        $correct_id = false;
        foreach ($kiosks as $row)
            if ($row->id === $kiosk->id)
                $correct_id = true;


        $correct_role = in_array($user->role, [
            Role::CUSTOMER,
            Role::ADMIN,
        ]);

        if (($correct_role && $correct_id) || $user->role === Role::ADMIN || $userKiosk->hasKiosk($user->id, $kiosek->id))
            return true;
        return false;
    }

    /**
     * Determine whether the user can update the kiosk.
     *
     * @param User $user
     * @return mixed
     */
    public function update(User $user)
    {
        return in_array($user->role, [
            Role::CUSTOMER,
            Role::ADMIN,
        ]);
    }

    /**
     * Determine whether the user can delete the kiosk.
     *
     * @param User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return in_array($user->role, [
            Role::ADMIN,
        ]);
    }

    /**
     * Determine whether the user can restore the kiosk.
     *
     * @param User $user
     * @return mixed
     */
    public function restore(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the kiosk.
     *
     * @param User $user
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        return in_array($user->role, [
            Role::ADMIN,
        ]);
    }
}

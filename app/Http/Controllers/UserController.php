<?php

namespace App\Http\Controllers;


use App\Enums\Role;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use App\Models\Kiosk;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * Get all users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAll() {
        $table = User::all();

        foreach ($table as $row) {
            if ($row->role === Role::ADMIN) {
                $row->role = 'Administrátor';
            }
            else {
                $row->role = 'Zákazník';
            }
        }

        return view('user.table', [ 'table' => $table ]);
    }

    /**
     * Return view for create page
     *
     * @param Kiosk $kiosk
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Kiosk $kiosk) {
        $kiosks = $kiosk->getAll();
        return view('user.create', [
            'kiosks' => $kiosks,
        ]);
    }

    /**
     * Store user to DB
     *
     * @param StoreUser $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreUser $request, User $user) {
        $request->validated();

        if ($user->create($request)) {
            flash('success')->success();
        }
        else {
            flash('error')->error();
        }

        return redirect()->route('user.getAll');
    }

    /**
     * Update user
     *
     * @param UpdateUser $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUser $request, User $user) {
        $request->validated();

        if ($user->updateById($request)) {
            flash('success')->success();
        }
        else {
            flash('error')->error();
        }

        return redirect()->route('user.getAll');
    }

    /**
     * Return view for edit page
     *
     * @param $user_id
     * @param User $user
     * @param Kiosk $kiosk
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($userId, User $user , Kiosk $kiosk) {
        $userResult = User::query()->findOrFail($userId);

        if ($userResult->active) {
            $userResult->active = 'Aktivní';
        }
        else {
            $userResult->active = 'Neaktivní';
        }

        if ($userResult->role === Role::ADMIN) {
            $userResult->role = 'Administrátor';
        }
        else {
            $userResult->role = 'Zákazník';
        }

        if (!$userResult->last_login) {
            $userResult->last_login = 'Nikdy';
        }

        $kiosks = $kiosk->getAll();
        $kioskIds = $user->getKioskIdsByUserId($userId);
        $userKiosks = [];
        foreach ($kioskIds as $item) { // Convert from array of ObjClass to array of integer
            array_push($userKiosks, $item->kiosk_id);
        }

        return view('user.edit', [
            'user' => $userResult,
            'kiosks' => $kiosks,
            'user_kiosks' => $userKiosks,
        ]);
    }

    /**
     * Delete user from DB
     *
     * @param int $id
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id, User $user) {
        $user->deleteById($id);
        return redirect()->route('user.getAll');
    }

}

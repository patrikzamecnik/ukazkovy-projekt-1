<?php

namespace App\Http\Controllers;

use App\Enums\Role;
use App\Models\Kiosk;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    const SHOW_ALL = "-1";

    /**
     * Return dates for transaction filter
     *
     * @return array
     */
    public function getDates() {
        return [
            'date_from' => Carbon::now()->firstOfMonth()->toDateString(),
            'date_to' => Carbon::now()->lastOfMonth()->toDateString()
        ];
    }

    /**
     * Get all transactions
     *
     * @param Transaction $transaction
     * @param Kiosk $kiosk
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAll(Transaction $transaction, Kiosk $kiosk) {

        $kiosksAll = $kiosk->getAll();
        $transactions = [];

        if (Auth::user()->role === Role::ADMIN) {
            $transactions = $transaction->getAll();
        }
        else {
            foreach ($kiosksAll as $item) {
                $transactionsByKioskId = $transaction->getAllByKioskId($item->id);
                if (!empty($transactionsByKioskId)) {
                    $transactions = array_merge($transactions, $transactionsByKioskId->toArray());
                }
            }
        }

        return view('transaction.table', [
            'table' => $transactions,
            'summary' => collect($transactions)->sum('amount'),
            'kiosks' => $kiosksAll,
            'dates' => $this->getDates(),
            'selected_kiosk_id' => 0,
        ]);
    }

    /**
     * Get transactions by kiosk ID
     *
     * @param int $kioskId
     * @param Transaction $transaction
     * @param Kiosk $kiosk
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAllByKioskId(int $kioskId, Transaction $transaction, Kiosk $kiosk) {

        $kiosksAll = $kiosk->getAll();
        $transactions = [];

        if ($kioskId) {
            $transactions = $transaction->getAllByKioskId($kioskId);
        }
        else {
            $kioskId = 0;
        }

        return view('transaction.table', [
            'table' => $transactions,
            'summary' => collect($transactions)->sum('amount'),
            'kiosks' => $kiosksAll,
            'dates' => $this->getDates(),
            'selected_kiosk_id' => $kioskId,
        ]);
    }

    /**
     * Get transactions by filter
     *
     * @param Request $request
     * @param Transaction $transaction
     * @param Kiosk $kiosk
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAllFiltered(Request $request, Transaction $transaction, Kiosk $kiosk) {

        $kiosksAll = $kiosk->getAll();
        $transactions = [];
        $dates = [
            'date_from' => $request->date_from,
            'date_to' => $request->date_to,
        ];

        if ($request->kiosk_id) {
            if ($request->kiosk_id === self::SHOW_ALL) {

                if (Auth::user()->role === Role::ADMIN) {
                    $transactions = $transaction->getAllFilteredAsAdmin($dates);
                }
                else {
                    $transactions = $transaction->getAllFilteredAsCustomer($kiosksAll, $dates);
                }
            }
            else {
                $transactions = $transaction->getByFilter($request);
            }
        }
        else {
            $request->kiosk_id = 0;
        }

        return view('transaction.table', [
            'table' => $transactions,
            'kiosks' => $kiosksAll,
            'summary' => collect($transactions)->sum('amount'),
            'dates' => $dates,
            'selected_kiosk_id' => $request->kiosk_id,
        ]);
    }
}

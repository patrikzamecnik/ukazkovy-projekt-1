<?php

namespace App\Http\Controllers;

use App\Enums\Role;
use App\Http\Requests\StoreKiosk;
use App\Http\Requests\UpdateKiosk;
use App\Models\Kiosk;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KioskController extends Controller
{

    /**
     * return create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('kiosk.create');
    }

    /**
     * Store kiosk in DB
     *
     * @param StoreKiosk $request
     * @param Kiosk $kiosk
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreKiosk $request, Kiosk $kiosk) {
        $request->validated();

        if ($kiosk->create($request)) {
            flash('success')->success();
        }
        else {
            flash('error')->error();
        }

        return redirect()->route('kiosk.getAll');
    }

    /**
     * Edit kiosk by ID
     *
     * @param Kiosk $kiosk
     * @param Transaction $transaction
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Kiosk $kiosk, Transaction $transaction)
    {
        $transactions = $transaction->getAllByKioskIdLimited($kiosk->id);

        return view('kiosk.edit', [
            'kiosk' => $kiosk,
            'transactions' => $transactions
        ]);
    }

    /**
     * Destroy kiosk by ID
     *
     * @param int $id
     * @param Kiosk $kiosk
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id, Kiosk $kiosk) {
        Kiosk::destroy($id);
        return redirect()->route('kiosk.getAll');
    }

    /**
     * Update kiosk
     *
     * @param UpdateKiosk $request
     * @param Kiosk $kiosk
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateKiosk $request, Kiosk $kiosk) {
        if (Auth::user()->role === Role::ADMIN) {
            $request->validated();
        }

        if ($kiosk->updateById($request)) {
            flash('success')->success();
        }
        else {
            flash('error')->error();
        }

        return redirect()->route('kiosk.getAll');
    }

    /**
     * Get all kiosks
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAll(User $user) {
        $kioskAll = Kiosk::all();
        $kioskIds = $user->getKioskIdsByUserId(Auth::user()->id);

        if (Auth::user()->role === Role::ADMIN)
            return view('kiosk.table', [ 'table' => $kioskAll ]);
        else {
            $kiosksByUser = [];
            foreach ($kioskAll as $row) {
                foreach ($kioskIds as $kiosk) {
                    if (strval($row->id) === strval($kiosk->kiosk_id)) {
                        array_push($kiosksByUser, $row);
                    }
                }
            }

            return view('kiosk.table', ['table' => $kiosksByUser]);
        }
    }
}

<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /** @var User $user */
        $user = $this->user;
        return [
            'password' => 'sometimes|confirmed',
            'email' => 'required|email|unique:users,email,'.$user->getKey(),
            'name' => 'required|unique:users,name,'.$user->getKey(),
            'surname' => 'required',
            'lastname' => 'required',
        ];
    }
}

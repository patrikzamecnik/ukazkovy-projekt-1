<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateKiosk extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|sometimes|unique:kiosks,code,' . $this->id,
            'name' => 'required',
            'location' => 'required',
            'amount' => 'required',
            'time_for_thanks' => 'required',
            'power_on_time' => 'required',
            'power_off_time' => 'required',
            'text_speed' => 'required',
        ];
    }
}

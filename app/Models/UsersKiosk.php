<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersKiosk extends Model
{
    protected $fillable = [
        'user_id', 'kiosk_id'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_kiosk';

    public function hasKiosk($userId, $kioskId) {
        $result = UsersKiosk::query()
            ->where('user_id', $userId)
            ->where('kiosk_id', $kioskId)
            ->get();
        if (count($result) !== 0)
            return true;
        return false;
    }
}

<?php

namespace App\Models;

use App\Enums\Role;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Kiosk extends Model
{
    protected $fillable = [
        'name', 'location', 'code', 'amount',
        'text_1', 'text_2', 'text_3', 'time_for_thanks',
        'text_speed', 'created_at', 'updated_at',
        'power_on_hour', 'power_off_hour', 'power_on_minute', 'power_off_minute'
    ];

    /**
     * Set the kiosk's power on hour column.
     *
     * @param  $value
     */
    public function setPowerOnHourAttribute($value)
    {
        $this->attributes['power_on_hour'] = date('H', strtotime($value));
    }

    /**
     * Set the kiosk's power on minute column.
     *
     * @param $value
     */
    public function setPowerOnMinuteAttribute($value) {
        $this->attributes['power_on_minute'] = date('i', strtotime($value));
    }

    /**
     * Set the kiosk's power off minute column.
     *
     * @param $value
     */
    public function setPowerOffMinuteAttribute($value) {
        $this->attributes['power_off_minute'] = date('i', strtotime($value));
    }

    /**
     * Set the kiosk's power off hour column.
     *
     * @param $value
     */
    public function setPowerOffHourAttribute($value) {
        $this->attributes['power_off_hour'] = date('H', strtotime($value));
    }

    /**
     * @param $value
     * @return int
     */
    public function getPowerOnHourAttribute($value) {
        if (strlen(strval($value)) === 1)
            return '0'.$value;
        else
            return $value;
    }

    /**
     * @param $value
     * @return int
     */
    public function getPowerOnMinuteAttribute($value) {
        if (strlen(strval($value)) === 1)
            return '0'.$value;
        else
            return $value;
    }

    /**
     * @param $value
     * @return int
     */
    public function getPowerOffMinuteAttribute($value) {
        if (strlen(strval($value)) === 1)
            return '0'.$value;
        else
            return $value;
    }

    /**
     * @param $value
     * @return int
     */
    public function getPowerOffHourAttribute($value) {
        if (strlen(strval($value)) === 1)
            return '0'.$value;
        else
            return $value;
    }

    /**
     * Get formatted created_at
     *
     * @param $value
     * @return
     */
    public function getCreatedAtAttribute($value) {
        return date('d.m.Y', strtotime($value));
    }

    /**
     * Get formatted battery_capacity
     *
     * @param $value
     * @return string
     */
    public function getBatteryCapacityAttribute($value) {
        return number_format($value, '3', '.', ' ');
    }

    /**
     * Get formatted current_battery_capacity
     *
     * @param $value
     * @return string
     */
    public function getCurrentBatteryCapacityAttribute($value) {
        return number_format($value, '3', '.', ' ');
    }

    /**
     * Get formatted current_battery_capacity
     *
     * @param $value
     * @return string
     */
    public function getBatteryVoltageAttribute($value) {
        return number_format($value, '3', '.', ' ');
    }

    /**
     * Insert kiosk to DB
     *
     * @param Request $request
     * @return bool
     */
    public function create(Request $request) {
        $kiosk = new Kiosk();
        $kiosk->fill($request->all());

        $kiosk->power_on_hour = $request->power_on_time;
        $kiosk->power_on_minute = $request->power_on_time;
        $kiosk->power_off_hour = $request->power_off_time;
        $kiosk->power_off_minute = $request->power_off_time;

        return $kiosk->save();
    }

    /**
     * update kiosk in DB
     *
     * @param Request $request
     * @return int
     */
    public function updateById(Request $request) {
        $kiosk = Kiosk::query()->findOrFail($request->id);
        $kiosk->fill($request->all());

        $kiosk->power_on_hour = $request->power_on_time;
        $kiosk->power_on_minute = $request->power_on_time;
        $kiosk->power_off_hour = $request->power_off_time;
        $kiosk->power_off_minute = $request->power_off_time;

        return $kiosk->save();
    }

    /**
     * Get all kiosks from DB
     *
     * @return array
     */
    public function getAll() {
        if (Auth::user()->role === Role::ADMIN) {
            return Kiosk::all();
        } else {
            $user = new User();
            $kiosksByUserId = $user->getKioskIdsByUserId(Auth::user()->id);
            $kiosks = [];
            foreach ($kiosksByUserId as $kiosk) {
                $result = Kiosk::where('kiosks.id', $kiosk->kiosk_id)
                                ->get();
                if ($result) {
                    array_push($kiosks, $result[0]);
                }
            }
            return $kiosks;
        }
    }

}

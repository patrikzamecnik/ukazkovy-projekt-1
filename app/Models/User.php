<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Notifications\PasswordReset;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'surname', 'lastname',
        'customer', 'email_verified_at', 'role', 'active',
        'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

    /**
     * Get the kiosks.
     */
    public function kiosks()
    {
        return $this->belongsToMany(Kiosk::class, 'users_kiosk', 'user_id', 'kiosk_id');
    }

    /**
     * Update user by ID
     *
     * @param Request $request
     * @return int
     */
    public function updateById(Request $request) {
        $this->fill($request->except('password'));

        if ($password = $request->input('password')) {
            $this->setAttribute('password', Hash::make($password));
        }

        DB::beginTransaction();

        $this->kiosks()->sync($request->input('kiosks', []));
        $success = $this->save();

        DB::commit();

        return $success;
    }

    /**
     * Delete user by ID
     *
     * @param int $id
     */
    public function deleteById(int $id) {
        User::destroy($id);
    }

    /**
     * Insert user to DB
     *
     * @param Request $request
     * @return bool
     */
    public function create(Request $request) {
        $user = new User();

        $user->fill($request->all());
        $user->password = Hash::make($request->password);

        $isInserted = $user->save();

        if ($isInserted) {
            $response = User::where('name', $request->name)
                                ->where('email', $request->email)
                                ->get();
            if ($response)
                if ($request->kiosks)
                    foreach ($request->kiosks AS $selected_kiosk_id) {
                        $users_kiosk = new UsersKiosk();

                        $users_kiosk->user_id = $response[0]->id;
                        $users_kiosk->kiosk_id = $selected_kiosk_id;

                        $users_kiosk->save();
                    }
        }
        return $isInserted;
    }

    /**
     * Get kiosks by user ID
     *
     * @param int $id
     * @return array
     */
    public function getKioskIdsByUserId(int $id) {
        return UsersKiosk::where('user_id', $id)
                            ->get();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Transaction extends Model
{
    protected $fillable = [
        'transaction_date', 'customer', 'card_name', 'card_number',
        'amount', 'kiosk_id', 'created_at', 'updated_at'
    ];

    /**
     * Get all transactions by kiosk ID from DB
     *
     * @param $kioskId
     * @return array
     */
    public function getAllByKioskId($kioskId) {
        return Transaction::where('transactions.kiosk_id', $kioskId)
                            ->join('kiosks', 'kiosks.id', '=', 'transactions.kiosk_id')
                            ->get();
    }

    /**
     * Get all transactions by kiosk code and date from DB (limited 5)
     *
     * @param int $kioskId
     * @return array
     */
    public function getAllByKioskIdLimited(int $kioskId) {
        return Transaction::where('transactions.kiosk_id', $kioskId)
                            ->orderBy('transactions.transaction_date', 'desc')
                            ->take(5)
                            ->get();
    }

    /**
     * Get all transactions
     *
     * @return array
     */
    public function getAll() {
        return Transaction::join('kiosks', 'kiosks.id', '=', 'transactions.kiosk_id')
                            ->get();
    }

    /**
     * Get all transactions by kiosk code and date from DB
     *
     * @param Request $request
     * @return array
     */
    public function getByFilter(Request $request) {
        return Transaction::where('transactions.kiosk_id', $request->kiosk_id)
            ->where('transactions.transaction_date', '>=', date('Y-m-d h:m:s', strtotime($request->date_from)))
            ->where('transactions.transaction_date', '<=', date('Y-m-d h:m:s', strtotime($request->date_to)))
            ->join('kiosks', 'kiosks.id', '=', 'transactions.kiosk_id')
            ->get();
    }

    /**
     * @param Transaction $transaction
     * @param $dates
     * @return array
     */
    public function getAllFilteredAsAdmin($dates) {
        $transactions = $this->getAll();
        $transactions = $transactions->reject(function($element) use ($dates) {
            return $element->transaction_date < $dates['date_from'] || $element->transaction_date > $dates['date_to'];
        });

        return $transactions;
    }

    public function getAllFilteredAsCustomer($kiosksAll, $dates) {
        $transactions = [];
        foreach ($kiosksAll as $item) {
            $transactionsByKioskId = $this->getAllByKioskId($item->id);
            if (!empty($transactionsByKioskId))
                $transactions = array_merge($transactions, $transactionsByKioskId->toArray());
        }

        $transactions = collect($transactions);
        $transactions = $transactions->reject(function($element) use ($dates) {
            return $element['transaction_date'] < $dates['date_from'] || $element['transaction_date'] > $dates['date_to'];
        });

        return $transactions;
    }
}
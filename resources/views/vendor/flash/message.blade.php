@foreach (session('flash_notification', collect())->toArray() as $message)
    @if ($message['overlay'])
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => $message['title'],
            'body'       => $message['message']
        ])
    @else
        @if ($message['level'] === 'success')
            <script>
                $.growl.notice({message: "Změny byly provedeny!"});
            </script>
        @else
            <script>
                $.growl.error({message: "Zkuste to prosím znovu."});
            </script>
        @endif
    @endif
@endforeach

{{ session()->forget('flash_notification') }}

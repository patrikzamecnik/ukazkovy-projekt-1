@extends('layouts.app_navbar_fixed')

@section('content')
    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

            <!-- Password recovery form -->
                <form class="login-form" method="POST" action="{{ route('password.email') }}">
                    @csrf

                    @if (session('status'))
                        <p class="alert alert-success">{{ session('status') }}</p>
                    @endif
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i class="icon-spinner11 icon-2x text-warning border-warning border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">Reset hesla</h5>
                                <span class="d-block text-muted">Instrukce vám budou zaslány na email</span>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-right">
                                <input id="email" name="email" type="email"
                                       class="form-control @error('email') is-invalid @enderror"
                                       placeholder="Vložte email" value="{{ old('email') }}" required
                                       autocomplete="email" autofocus>
                                <div class="form-control-feedback">
                                    <i class="icon-mail5 text-muted"></i>
                                </div>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <button type="submit" class="btn bg-purple-400 btn-block"><i
                                        class="icon-spinner11 mr-2"></i> Resetovat heslo
                            </button>
                        </div>
                    </div>
                </form>
                <!-- /password recovery form -->
            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
@endsection

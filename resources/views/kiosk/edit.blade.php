@extends('layouts.app_navbar_fixed')

@section('content')
    @include('layouts.sidebar')
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-pencil mr-2"></i> Nastavení kiosku</h4>
                </div>

                <div class="header-elements d-none">
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('kiosk.getAll') }}" class="btn btn-link btn-float text-purple-400">
                            <i class="icon-arrow-left52 text-purple-400"></i><span>Zpět</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page header -->

        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    {!! Form::open( ['route' => ['kiosk.update', 'id' => $kiosk->id]] ) !!}
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Nastavení kiosku</h5>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row justify-content-left">
                                <div class="form-group col-sm">
                                    <label for="name">Název kiosku</label>
                                    <input type="text" class="form-control" id="name"
                                           aria-describedby="name" placeholder="Vložte název kiosku" name="name"
                                           value="{{ $kiosk->name }}" required>
                                </div>
                                <div class="form-group col-sm">
                                    <label for="location">Umístění kiosku</label>
                                    <input type="text" class="form-control" id="location"
                                           placeholder="Vložte umístění kiosku" name="location"
                                           value="{{ $kiosk->location }}"
                                           required>
                                </div>
                                <div class="form-group col-sm">
                                    <label for="code">Kód kiosku</label>
                                    @if (Auth::user()->role === \App\Enums\Role::ADMIN)
                                        <input type="text" class="form-control" id="code"
                                               placeholder="Vložte kód kiosku" name="code" value="{{ $kiosk->code }}">
                                    @else
                                        <input type="text" class="form-control" id="code"
                                               placeholder="Vložte kód kiosku" value="{{ $kiosk->code }}" disabled>
                                    @endif
                                </div>
                            </div>
                            <div class="row justify-content-left">
                                <div class="form-group col-sm">
                                    <label for="amount">Částka</label>
                                    <input type="number" class="form-control" id="amount"
                                           aria-describedby="amount" placeholder="Vložte částku" name="amount"
                                           value="{{ $kiosk->amount }}" required>
                                </div>
                                <div class="form-group col-sm">
                                    <label for="time_for_thanks">Délka zobrazení děkovací hlášky</label>
                                    <input type="number" class="form-control" id="time_for_thanks"
                                           placeholder="30 sekund" name="time_for_thanks"
                                           value="{{ $kiosk->time_for_thanks }}"
                                           required>
                                </div>
                            </div>
                            <div class="row justify-content-left">
                                <div class="form-group col-sm">
                                    <label for="power_on_time">Nastavení času pro zapnutí</label>
                                    <input type="time" class="form-control" id="power_on_time"
                                           aria-describedby="power_on_time" name="power_on_time"
                                           value="{{ $kiosk->power_on_hour }}:{{  $kiosk->power_on_minute }}" required>
                                </div>
                                <div class="form-group col-sm">
                                    <label for="power_off_time">Nastavení času pro vypnutí</label>
                                    <input type="time" class="form-control" id="power_off_time"
                                           aria-describedby="power_off_time" name="power_off_time"
                                           value="{{ $kiosk->power_off_hour }}:{{  $kiosk->power_off_minute }}" required>
                                </div>
                            </div>
                            <div class="row justify-content-left">
                                <div class="form-group col-sm">
                                    <label for="text_1">Text 1</label>
                                    <textarea class="form-control" rows="5" id="text_1" name="text_1">{{ $kiosk->text_1 }}</textarea>
                                </div>

                                <div class="form-group col-sm">
                                    <label for="text_2">Text 2</label>
                                    <textarea class="form-control" rows="5" id="text_2" name="text_2">{{ $kiosk->text_2 }}</textarea>
                                </div>

                                <div class="form-group col-sm">
                                    <label for="text_3">Text 3</label>
                                    <textarea class="form-control" rows="5" id="text_3" name="text_3">{{ $kiosk->text_3 }}</textarea>
                                </div>
                            </div>
                            <div class="row justify-content-left">
                                <div class="form-group col-sm-6">
                                    <label for="text_speed">Rychlost textu</label>
                                    <input type="number" class="form-control" id="text_speed"
                                           aria-describedby="text_speed" placeholder="800 milisekund" name="text_speed"
                                           value="{{ $kiosk->text_speed }}" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex justify-content-between">
                        <div>
                            <button type="submit" class="btn bg-purple-400">
                                <i class="icon-floppy-disk mr-2"></i> Uložit změny
                            </button>
                            <a href="{{ URL::previous() }}" class="btn bg-grey-400">
                                Zpět
                            </a>
                        </div>

                        @if(Auth::user()->role === \App\Enums\Role::ADMIN)
                            <a href="{{ route('kiosk.delete', $kiosk->id) }}" class="delete">
                                <span id="del" class="btn btn-danger">Odstranit kiosek</span>
                            </a>
                        @endif
                    </div>

                    {!! Form::close() !!}
                </div>
                <div class="col-sm-4">
                    <div class="content-wrapper">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title">Doplňující údaje</h5>
                            </div>
                            <div class="card-body">
                                <div class="row justify-content-left">
                                    <div class="form-group col-sm">
                                        <label for="battery_voltage">Napětí baterie</label>
                                        <input type="text" class="form-control" id="battery_voltage"
                                               aria-describedby="battery_voltage" name="battery_voltage"
                                               value="{{ $kiosk->battery_voltage }} mV" disabled>
                                    </div>
                                    <div class="form-group col-sm">
                                        <label for="current_battery_capactity">Aktuální kapacita</label>
                                        <input type="text" class="form-control" id="current_battery_capactity"
                                               aria-describedby="current_battery_capactity"
                                               name="current_battery_capactity"
                                               value="{{ $kiosk->current_battery_capacity }} mAh" disabled>
                                    </div>
                                </div>
                                <div class="row justify-content-left">
                                    <div class="form-group col-sm">
                                        <label for="battery_capacity">Celková kapacita</label>
                                        <input type="text" class="form-control" id="battery_capacity"
                                               aria-describedby="battery_capacity" name="battery_capacity"
                                               value="{{ $kiosk->battery_capacity }} mV" disabled>
                                    </div>
                                    <div class="form-group col-sm">
                                        <label for="current_battery_capactity">Aktuální kapacita</label>
                                        <input type="text" class="form-control" id="current_battery_capactity"
                                               aria-describedby="current_battery_capactity"
                                               name="current_battery_capactity"
                                               value="{{ $kiosk->current_battery_capacity }} mAh" disabled>
                                    </div>
                                </div>
                                <div class="row justify-content-left">
                                    <div class="form-group col-sm">
                                        <label for="status">Status</label>
                                        <input type="text" class="form-control" id="status"
                                               aria-describedby="status" name="status" value="{{ $kiosk->status }}"
                                               disabled>
                                    </div>
                                    <div class="form-group col-sm">
                                        <label for="alarm">Alarm</label>
                                        <input type="text" class="form-control" id="alarm"
                                               aria-describedby="alarm" name="alarm" value="{{ $kiosk->alarm }}" disabled>
                                    </div>
                                </div>
                                <div class="row justify-content-left">
                                    <div class="form-group col-sm">
                                        <label for="safe">Safe</label>
                                        <input type="text" class="form-control" id="safe"
                                               aria-describedby="safe" name="safe" value="{{ $kiosk->safe }}" disabled>
                                    </div>
                                    <div class="form-group col-sm">
                                        <label for="created_at">Datum vytvoření</label>
                                        <input type="text" class="form-control" id="created_at"
                                               aria-describedby="created_at" name="created_at"
                                               value="{{ $kiosk->created_at }}"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($transactions)
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title">Poslední transakce</h5>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            @if(!isset($transactions) || $transactions->count() == 0)
                                                <p class="text-muted">U kiosku nejsou evidovány žádné transakce</p>
                                            @else
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>Datum</th>
                                                        <th>Částka</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($transactions as $transaction)
                                                        <tr>
                                                            <td>{{ date('d.m.Y', strtotime($transaction->transaction_date)) }}</td>
                                                            <td>{{ number_format($transaction->amount, '0', ',', '.') }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                <a href="{{ route('transaction.getAllByKioskId', $kiosk->id) }}">
                                                    <span class="btn btn-primary">Všechny transakce</span>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script>
        $(".delete").on("click", function () {
            return confirm("Opravdu chcete smazat kiosek?");
        });
    </script>
@endsection

@extends('layouts.app_navbar_fixed')

@section('content')
    @include('layouts.sidebar')
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-add mr-2"></i> Přidat kiosek</h4>
                </div>

                <div class="header-elements d-none">
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('kiosk.getAll') }}" class="btn btn-link btn-float text-purple-400">
                            <i class="icon-arrow-left52 text-purple-400"></i><span>Zpět</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <!-- /page header -->

        <!-- Content area -->
        <div class="content">
            {!! Form::open( ['route' => 'kiosk.store'] ) !!}

            <div class="card">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-body">
                    <div class="row justify-content-left">
                        <div class="form-group col-sm">
                            <label for="name">Název kiosku</label>
                            <input type="text" class="form-control" id="name"
                                   aria-describedby="name" placeholder="Vložte název kiosku" name="name" required>
                        </div>
                        <div class="form-group col-sm">
                            <label for="location">Umístění kiosku</label>
                            <input type="text" class="form-control" id="location"
                                   placeholder="Vložte umístění kiosku" name="location" required>
                        </div>
                        <div class="form-group col-sm">
                            <label for="code">Kód kiosku</label>
                            <input type="text" class="form-control" id="code"
                                   placeholder="Vložte kód kiosku" name="code" required>
                        </div>
                    </div>
                    <div class="row justify-content-left">
                        <div class="form-group col-sm">
                            <label for="amount">Částka</label>
                            <input type="number" class="form-control" id="amount"
                                   aria-describedby="amount" placeholder="Vložte částku" name="amount" required>
                        </div>
                        <div class="form-group col-sm">
                            <label for="time_for_thanks">Délka zobrazení děkovací hlášky</label>
                            <input type="number" class="form-control" id="time_for_thanks"
                                   placeholder="30 sekund" name="time_for_thanks" required>
                        </div>
                    </div>
                    <div class="row justify-content-left">
                        <div class="form-group col-sm">
                            <label for="power_on_time">Nastavení času pro zapnutí</label>
                            <input type="time" class="form-control" id="power_on_time"
                                   aria-describedby="power_on_time" name="power_on_time" required>
                        </div>
                        <div class="form-group col-sm">
                            <label for="power_off_time">Nastavení času pro vypnutí</label>
                            <input type="time" class="form-control" id="power_off_time"
                                   aria-describedby="power_off_time" name="power_off_time" required>
                        </div>
                    </div>
                    <div class="row justify-content-left">
                        <div class="form-group col-sm">
                            <label for="text_1">Text 1</label>
                            <textarea class="form-control" rows="5" id="text_1" name="text_1"></textarea>
                        </div>

                        <div class="form-group col-sm">
                            <label for="text_2">Text 2</label>
                            <textarea class="form-control" rows="5" id="text_2" name="text_2"></textarea>
                        </div>

                        <div class="form-group col-sm">
                            <label for="text_3">Text 3</label>
                            <textarea class="form-control" rows="5" id="text_2" name="text_3"></textarea>
                        </div>
                    </div>
                    <div class="row justify-content-left">
                        <div class="form-group col-sm-6">
                            <label for="text_speed">Rychlost textu</label>
                            <input type="number" class="form-control" id="text_speed"
                                   aria-describedby="text_speed" placeholder="800 milisekund" name="text_speed" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-between">
                <button type="submit" class="btn bg-purple-400">
                    <i class="icon-floppy-disk mr-2"></i> Uložit
                </button>
                <a href="{{ URL::previous() }}" class="btn bg-grey-400">
                    Zpět
                </a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection

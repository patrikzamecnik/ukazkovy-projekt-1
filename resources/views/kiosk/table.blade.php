@extends('layouts.app_navbar_fixed')

@section('content')
    @include('layouts.sidebar')
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-list-unordered mr-2"></i> Seznam kiosků</h4>
                </div>

                @if (Auth::user()->role == \App\Enums\Role::ADMIN)
                    <div class="header-elements d-none">
                        <div class="d-flex justify-content-center">
                            <a href="{{ route('kiosk.create') }}" class="btn btn-link btn-float text-purple-400"><i
                                    class="icon-add text-purple-400"></i><span>Přidat kiosek</span></a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <!-- /page header -->

        <!-- Content area -->
        <div class="content">
            @include('flash::message')

            <div class="mb-3">
                <h6 class="mb-0 font-weight-semibold">
                    Celkový počet kiosků: {{ count($table) }}
                </h6>
            </div>

            <div class="row">
                @foreach($table as $kiosk)
                    <div class="col-sm-4">
                        <div class="card border-left-3 border-left-purple-400">
                            <div class="card-header header-elements-inline">
                                <h4 class="card-title">{{ $kiosk->name }}</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="mb-2">
                                            <span class="font-weight-semibold"><i class="icon-price-tag2 mr-2"></i>Kód</span>
                                            <span class="font-size-base text-muted d-block">{{ $kiosk->code }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-2">
                                            <span class="font-weight-semibold"><i class="icon-coins mr-2"></i>Částka</span>
                                            <span class="font-size-base text-muted d-block">{{ number_format($kiosk->amount, '0', ',', '.') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-2">
                                            <span class="font-weight-semibold"><i class="icon-battery-6 mr-2"></i>Baterie</span>
                                            <span class="font-size-base text-muted d-block">{{ number_format($kiosk->current_battery_capacity, '0', ',', ' ') }}/{{ number_format($kiosk->battery_capacity, '0', ',', ' ') }} mAh</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-2">
                                    <span class="font-weight-semibold"><i class="icon-pin mr-2"></i>Umístění</span>
                                    <span class="font-size-base text-muted d-block">{{ $kiosk->location }}</span>
                                </div>

                            </div>
                            <div class="card-footer bg-white d-flex justify-content-between">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="{{ route('kiosk.edit', $kiosk->id) }}" class="btn-link text-purple-400">
                                            <i class="icon-pencil mr-2"></i>Nastavení kiosku
                                        </a>
                                    </li>
                                </ul>

                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        <a href="{{ route('transaction.getAllByKioskId', $kiosk->id) }}"
                                           class="btn-link text-purple-400">
                                            <i class="icon-calculator2 mr-2"></i>Seznam transakcí</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            @if (Auth::user()->role == \App\Enums\Role::ADMIN)
                <a href="{{ route('kiosk.create') }}" class="btn bg-purple-400">
                    <i class="icon-add mr-2"></i> Přidat kiosek
                </a>
            @endif
        </div>
    </div>
@endsection

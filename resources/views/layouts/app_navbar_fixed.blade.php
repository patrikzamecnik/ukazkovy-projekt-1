<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Charitativní kiosek / administrace') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Favicons -->
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png"/>
    <link rel="icon" type="image/png" sizes="48x48" href="/images/favicon-48x48.png"/>
    <link rel="apple-touch-icon" type="image/png" sizes="57x57" href="/images/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon" type="image/png" sizes="60x60" href="/images/apple-touch-icon-60x60.png"/>
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="/images/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" type="image/png" sizes="76x76" href="/images/apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon" type="image/png" sizes="114x114" href="/images/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon" type="image/png" sizes="120x120" href="/images/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="/images/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon" type="image/png" sizes="152x152" href="/images/apple-touch-icon-152x152.png"/>
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="/images/apple-touch-icon-180x180.png"/>
    <!-- /favicons -->

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link rel='stylesheet' href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/css/global/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/components.min.css" rel="stylesheet" type="text/css">
    <link href="/css/assets/colors.min.css" rel="stylesheet" type="text/css">
    <link href="/css/jquery.growl.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="/js/global/js/main/jquery.min.js"></script>
    <script src="/js/global/js/main/bootstrap.bundle.min.js"></script>
    <script src="/js/global/js/plugins/loaders/blockui.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="/js/global/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/js/global/js/plugins/forms/selects/select2.min.js"></script>

    <script src="/js/assets/app.js"></script>


    <script src="/js/global/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/js/global/js/demo_pages/form_multiselect.js"></script>
    <!-- /theme JS files -->

    @yield ('header_scripts')

    <script src="/js/jquery.growl.js" type="text/javascript"></script>
    <link href="/css/jquery.growl.css" rel="stylesheet" type="text/css" />
</head>
<body class="navbar-top">
    <div class="navbar navbar-dark navbar-expand-md fixed-top">
        <div class="navbar-brand">
            <a href="/" class="d-inline-block">
                <img src="/images/logo_charitativni_kiosek-white.svg" alt="logo_charitativni_kiosek-white.svg">
            </a>
        </div>

        <div class="d-xl-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-demo3-mobile">
                <i class="icon-grid3"></i>
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-demo3-mobile">
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link text-light" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                @else
                    <li class="nav-item dropdown dropdown-user">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <img src="/images/avatar-placeholder.jpg" class="rounded-circle mr-2" height="30" alt="">
                            <b>{{ Auth::user()->surname }} {{ Auth::user()->lastname }}</b>
                            <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
    <div class="page-content">
        @yield('content')
    </div>

    @yield ('footer_scripts')

</body>
</html>

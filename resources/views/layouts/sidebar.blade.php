<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">
        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="/images/avatar-placeholder.jpg" width="38" height="38"
                                         class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div
                            class="media-title font-weight-semibold">{{ Auth::user()->surname }} {{ Auth::user()->lastname }}</div>
                        <div class="font-size-xs opacity-50">
                            @if (Auth::user()->role === \App\Enums\Role::ADMIN)
                                Administrátor
                            @else
                                Zákazník
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Administrace</div>
                    <i class="icon-menu" title="Main"></i></li>

                <li class="nav-item">
                    <a href="{{route('transaction.getAll')}}" class="nav-link text-light">
                        <i class="icon-calculator2"></i>
                        <span>
									Seznam transakcí
								</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('kiosk.getAll')}}" class="nav-link text-light">
                        <i class="icon-list-unordered"></i>
                        <span>
									Seznam kiosků
								</span>
                    </a>
                </li>
                @if(Auth::user()->role === \App\Enums\Role::ADMIN)
                    <li class="nav-item">
                        <a href="{{route('user.getAll')}}" class="nav-link text-light">
                            <i class="icon-users"></i>
                            <span>
									Správa uživatelů
								</span>
                        </a>
                    </li>
                @endif

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>

@extends('layouts.app_navbar_fixed')

@section('content')
    @include('layouts.sidebar')
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-pencil mr-2"></i> Nastavení uživatele</h4>
                </div>

                <div class="header-elements d-none">
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('user.getAll') }}" class="btn btn-link btn-float text-purple-400">
                            <i class="icon-arrow-left52 text-purple-400"></i><span>Zpět</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page header -->

        <div class="content">
            <div class="row">
                <div class="col-8">
                    {!! Form::open( ['url' => route('user.update', $user->id), 'method' => 'PATCH'] ) !!}
                    <div class="card">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-header">
                            <h5 class="card-title">Detail uživatele</h5>
                        </div>
                        <div class="card-body">
                            <div class="row justify-content-left">
                                <div class="form-group col-sm">
                                    <label for="name">Uživatelské jméno</label>
                                    <input type="text" class="form-control" id="name"
                                           aria-describedby="name" name="name"
                                           value="{{ $user->name }}" required>
                                </div>
                                <div class="form-group col-sm">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email"
                                           aria-describedby="email" name="email" value="{{ $user->email }}" required>
                                </div>

                            </div>
                            <div class="row justify-content-left">
                                <div class="form-group col-sm">
                                    <label for="surname">Jméno</label>
                                    <input type="text" class="form-control" id="surname"
                                           aria-describedby="surname" name="surname" value="{{ $user->surname }}" required>
                                </div>
                                <div class="form-group col-sm">
                                    <label for="lastname">Příjmení</label>
                                    <input type="text" class="form-control" id="lastname"
                                           name="lastname" value="{{ $user->lastname }}" required>
                                </div>
                            </div>
                            <div class="row justify-content-left">
                                <div class="form-group col-sm">
                                    <label for="password">Heslo</label>
                                    <input type="password" class="form-control" id="password"
                                           aria-describedby="password" name="password">
                                </div>
                                <div class="form-group col-sm">
                                    <label for="password_confirmation">Potvrdit heslo</label>
                                    <input type="password" class="form-control" id="password_confirmation"
                                           aria-describedby="password_confirmation" name="password_confirmation">
                                </div>
                            </div>
                            {{--
                            <div class="form-group col-sm">
                                <label for="customer">Název subjektu</label>
                                <input type="text" class="form-control" id="customer"
                                       aria-describedby="customer" name="customer" value="{{$user->customer}}"
                                       hidden>
                            </div>
                            --}}
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="row justify-content-left">
                                <div class="form-group col-sm">
                                    <label for="role">Role</label>
                                    <select class="form-control" id="role" name="role">
                                        @if ($user->role === 'Administrátor')
                                            <option value="admin" selected>Administrátor</option>
                                            <option value="customer">Zákazník</option>
                                        @else
                                            <option value="admin">Administrátor</option>
                                            <option value="customer" selected>Zákazník</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-sm">
                                    <label for="active">Aktivní / Neaktivní</label>
                                    <select class="form-control" id="active" name="active">
                                        @if ($user->active === 'Aktivní')
                                            <option value=1 selected>Aktivní</option>
                                            <option value=0>Neaktivní</option>
                                        @else
                                            <option value=1>Aktivní</option>
                                            <option value=0 selected>Neaktivní</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row justify-content-left">
                                <div class="form-group col-sm">
                                    <label>Přiřadit kiosek</label>
                                    <div class="multiselect-native-select">
                                        <select class="form-control multiselect-filtering" multiple="multiple"
                                                data-fouc="" name="kiosks[]">
                                            @foreach($kiosks as $kiosk)
                                                @if (in_array($kiosk->id, $user_kiosks))
                                                    <option value="{{ $kiosk->id }}" selected>#{{ $kiosk->code }}</option>
                                                @else
                                                    <option value="{{ $kiosk->id }}">#{{ $kiosk->code }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex justify-content-between">
                        <div>
                            <button type="submit" class="btn bg-purple-400">
                                <i class="icon-floppy-disk mr-2"></i> Uložit změny
                            </button>
                            <a href="{{ URL::previous() }}" class="btn bg-grey-400">
                                Zpět
                            </a>
                        </div>

                        <a href="{{ route('user.delete', $user->id) }}" class="delete">
                            <span id="del" class="btn btn-danger">Odstranit uživatele</span>
                        </a>
                    </div>

                    {!! Form::close() !!}
                </div>
                <div class="col-4">
                    <div class="content-wrapper">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title">Doplňující údaje</h5>
                            </div>

                            <div class="card-body">
                                <div class="row justify-content-left">
                                    <div class="form-group col-sm">
                                        <label for="created_at">Datum vytvoření</label>
                                        <input type="text" class="form-control" id="created_at"
                                               aria-describedby="created_at" name="created_at"
                                               value="{{ $user->created_at }}"
                                               disabled>
                                    </div>
                                    <div class="form-group col-sm">
                                        <label for="last_login">Poslední přihlášení</label>
                                        <input type="text" class="form-control" id="last_login"
                                               aria-describedby="last_login" name="last_login"
                                               value="{{ $user->last_login }}"
                                               disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('footer_scripts')
            <script>
                $(".delete").on("click", function () {
                    return confirm("Opravdu chcete smazat uživatele?");
                });
            </script>
@endsection

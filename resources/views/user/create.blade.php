@extends('layouts.app_navbar_fixed')

@section('content')
    @include('layouts.sidebar')
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-add mr-2"></i> Přidat uživatele</h4>
                </div>

                <div class="header-elements d-none">
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('user.getAll') }}" class="btn btn-link btn-float text-purple-400">
                            <i class="icon-arrow-left52 text-purple-400"></i><span>Zpět</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page header -->

        <div class="content">
            {!! Form::open( ['route' => 'user.store'] ) !!}
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row justify-content-left">
                        <div class="form-group col-sm">
                            <label for="name">Uživatelské jméno</label>
                            <input type="text" class="form-control" id="name"
                                   aria-describedby="name" placeholder="Vložte uživatelské jméno" name="name"
                                   required>
                        </div>

                        <div class="form-group col-sm">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email"
                                   aria-describedby="email" name="email" placeholder="Vložte email" required>
                        </div>
                    </div>
                    <div class="row justify-content-left">
                        <div class="form-group col-sm">
                            <label for="surname">Jméno</label>
                            <input type="text" class="form-control" id="surname"
                                   aria-describedby="surname" placeholder="Vložte jméno" name="surname"
                                   required>
                        </div>
                        <div class="form-group col-sm">
                            <label for="lastname">Příjmení</label>
                            <input type="text" class="form-control" id="lastname"
                                   placeholder="Vložte příjmení" name="lastname" required>
                        </div>
                    </div>
                    <div class="row justify-content-left">
                        <div class="form-group col-sm-6">
                            <label for="password">Heslo</label>
                            <input type="password" class="form-control" id="password"
                                   aria-describedby="password" placeholder="Vložte heslo" name="password"
                                   required>
                        </div>
                    </div>
                    {{--
                    <div class="form-group col-sm">
                        <label for="customer" hidden>Název subjektu</label>
                        <input type="text" class="form-control" id="customer"
                               aria-describedby="customer" name="customer"
                               placeholder="Vložte název subjektu">
                    </div>
                    --}}
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row justify-content-left">
                        <div class="form-group col-sm">
                            <label for="role">Role</label>
                            <select class="form-control" id="role" name="role">
                                <option value="admin">Administrátor</option>
                                <option value="customer">Zákazník</option>
                            </select>
                        </div>
                        <div class="form-group col-sm">
                            <label for="active">Aktivní / Neaktivní</label>
                            <select class="form-control" id="active" name="active">
                                <option value=1>Aktivní</option>
                                <option value=0>Neaktivní</option>
                            </select>
                        </div>
                    </div>
                    <div class="row justify-content-left">
                        <div class="form-group col-sm">
                            <label>Přiřadit kiosek</label>
                            <div class="multiselect-native-select">
                                <select class="form-control multiselect-filtering" multiple="multiple"
                                        data-fouc="" name="kiosks[]">
                                    @foreach($kiosks as $kiosk)
                                        <option value="{{ $kiosk->id }}">#{{ $kiosk->code }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-between">
                <button type="submit" class="btn bg-purple-400">
                    <i class="icon-floppy-disk mr-2"></i> Uložit
                </button>
                <a href="{{ URL::previous() }}" class="btn bg-grey-400">
                    Zpět
                </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection

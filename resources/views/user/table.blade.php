@extends('layouts.app_navbar_fixed')

@section('header_scripts')
    <script src="/js/global/js/datatables/user_datatables_basic.js"></script>
@endsection

@section('content')
    @include('layouts.sidebar')

    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-list-unordered mr-2"></i> Správa uživatelů</h4>
                </div>

                @if (Auth::user()->role == \App\Enums\Role::ADMIN)
                    <div class="header-elements d-none">
                        <div class="d-flex justify-content-center">
                            <a href="{{ route('user.create') }}" class="btn btn-link btn-float text-purple-400"><i
                                    class="icon-add text-purple-400"></i><span>Přidat uživatele</span></a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <!-- /page header -->

        <!-- Content area -->
        <div class="content">
            <div class="card">
                <div class="card-body">
                    @include('flash::message')

                    <table class="table datatable-basic table-hover">
                        <thead>
                        <tr>
                            <th>Uživatelské jméno</th>
                            <th>Kontaktní osoba</th>
                            <th>E-mailová adresa</th>
                            <th>Role</th>
                            <th>Uživatelem od</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($table as $row)
                            <tr>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->surname }} {{ $row->lastname }}</td>
                                <td>{{ $row->email }}</td>
                                <td>{{ $row->role }}</td>
                                <td>{{ $row->created_at }}</td>
                                <td>{{ $row->active ? 'Aktivní' : 'Neaktivní' }}</td>
                                <td class="text-right">
                                    <a href="{{ route('user.edit', $row->id) }}"
                                       class="btn-link text-purple-400">
                                        <i class="icon-pencil mr-2"></i>Nastavení uživatele</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            @if (Auth::user()->role == \App\Enums\Role::ADMIN)
                <a href="{{ route('user.create') }}" class="btn bg-purple-400">
                    <i class="icon-add mr-2"></i> Přidat uživatele
                </a>
            @endif

        </div>
    </div>
@endsection

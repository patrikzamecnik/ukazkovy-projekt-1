@extends('layouts.app_navbar_fixed')

@section('header_scripts')
    <script src="/js/global/js/datatables/transaction_datatables_basic.js"></script>
@endsection

@section('content')
    @include('layouts.sidebar')
    <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-calculator2 mr-2"></i> Seznam transakcí</h4>
                </div>
            </div>
        </div>
        <!-- /page header -->

        <!-- Content area -->
        <div class="content">
            <div class="card">
                <div class="card-body">

                    <fieldset>
                        <legend class="font-weight-semibold"><i class="icon-search4 mr-2"></i> Filtr</legend>

                        {!! Form::open( ['route' => 'transaction.getAllFiltered'] ) !!}
                        <div class="row justify-content-left">
                            <div class="form-group col-lg-2">
                                <label for="date_from">Datum od:</label>
                                <input type="date" class="form-control" id="date_from"
                                       aria-describedby="date_from" name="date_from"
                                       value="{{$dates['date_from']}}" required>
                            </div>
                            <div class="form-group col-lg-2">
                                <label for="date_to">Datum do:</label>
                                <input type="date" class="form-control" id="date_to"
                                       aria-describedby="date_to" name="date_to"
                                       value="{{$dates['date_to']}}" required>
                            </div>
                            <div class="form-group col-lg-2">
                                <label for="kiosk_id">Kiosk:</label>
                                <select class="form-control" id="kiosk_id" name="kiosk_id">
                                    <option value=-1>Zobrazit všechny</option>
                                    @foreach($kiosks as $kiosk)
                                        @if (strval($kiosk->id) === strval($selected_kiosk_id))
                                            <option value="{{$kiosk->id}}" selected>#{{$kiosk->code}}</option>
                                        @else
                                            <option value="{{$kiosk->id}}">#{{$kiosk->code}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <div style="margin-top: 28px;">
                                    <button type="submit" class="btn bg-purple-400">
                                        Hledat
                                    </button>
                                    <a href="{{ route('transaction.getAll') }}">
                                        <span class="btn bg-grey-300">Zrušit filtr</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </fieldset>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <table class="table datatable-basic table-hover">
                        <thead>
                        <tr>
                            <th>Datum</th>
                            <th>Název karty</th>
                            <th>Číslo karty</th>
                            <th>Částka</th>
                            <th>Kód kiosku</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($table as $row)
                            <tr>
                                <td>{{$row['transaction_date']}}</td>
                                <td>{{$row['card_name']}}</td>
                                <td>{{$row['card_number']}}</td>
                                <td>{{number_format($row['amount'], '0', ',', '.')}}</td>
                                <td>#{{$row['code']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-center">
                    <form>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-5 col-form-label">Součet transakcí:</label>
                            <div class="col-sm-5">
                                <input type="amount" class="form-control" id="amount" value="{{$summary}}" readonly>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

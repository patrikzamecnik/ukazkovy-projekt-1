<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('transaction_date');
            $table->string('customer', 100)->nullable();
            $table->string('card_name', 100)->nullable();
            $table->string('card_number', 20)->nullable();
            $table->decimal('amount', 10, 2)->nullable();
            $table->unsignedBigInteger('kiosk_id');
            $table->timestamps();

            $table->foreign('kiosk_id')->references('id')->on('kiosks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}

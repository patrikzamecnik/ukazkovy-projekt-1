<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKiosksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kiosks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('location');
            $table->string('code')->unique();
            $table->decimal('battery_voltage', 5, 3)->nullable();
            $table->decimal('current_battery_capacity', 5, 3)->nullable();
            $table->decimal('battery_capacity', 5, 3)->nullable();
            $table->integer('status')->nullable();
            $table->integer('alarm')->nullable();
            $table->integer('safe')->nullable();
            $table->decimal('amount', 10, 2);
            $table->string('text_1')->nullable();
            $table->string('text_2')->nullable();
            $table->string('text_3')->nullable();
            $table->integer('power_on_hour')->nullable();
            $table->integer('power_on_minute')->nullable();
            $table->integer('power_off_hour')->nullable();
            $table->integer('power_off_minute')->nullable();
            $table->integer('time_for_thanks')->nullable();
            $table->integer('text_speed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kiosks');
    }
}

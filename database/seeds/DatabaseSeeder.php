<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(User $user)
    {
        $user->name = 'admin';
        $user->password = Hash::make('password');
        $user->surname = Str::random(10);
        $user->lastname = Str::random(10);
        $user->email = Str::random(10).'@gmail.com';
        $user->role = 'admin';
        $user->active = true;
        $user->save();
    }
}
